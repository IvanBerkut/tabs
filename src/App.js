import React from 'react';
import Tabs from "./components/Tabs";
import './App.css';

function App() {
  return (
    <div className="App">
      <Tabs> 
       <div label="Tab One"> 
         Tab One Content! 
       </div> 
       <div label="Tab Two"> 
          Tab Two Content!
       </div> 
       <div label="Tab Three"> 
          Tab Three Content!
       </div> 
     </Tabs> 
    </div>
  );
}

export default App;
